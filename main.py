import time
import pygame
import requests
from requests import Session
import json
from pygame.locals import *

pygame.init()

WINDOWSIZE = (500, 500)
URL = "https://api3.binance.com/api/v3/ticker/price?symbol=BTCEUR"
DISTANCE = 50
NBR = 50
FONT = pygame.font.SysFont(None, 24)

fenetre = pygame.display.set_mode(WINDOWSIZE)
pygame.display.set_caption("cours du bitcoin")
mainClock = pygame.time.Clock()

framerate = 60
time_per_frame = 20 / 60
last_time = time.time()
frameCount = 0

session = Session()
run = True
listePrice = []

def drawText(text, color, x, y, center=False):
    text = FONT.render(text, True, color)
    if center:
        text_rect = text.get_rect(center=(x, y))
        fenetre.blit(text, text_rect)
    else:
        fenetre.blit(text, (x, y))

def heightByLimit(value):
    newValue = (float(listePrice[i]) - btcMin) / (btcMax - btcMin)
    denum = WINDOWSIZE[0]
    r = -0 / denum
    s = 1 / denum
    return (newValue - r) / s

def getColorBetweenPrice(price1, price2):
    res = ()
    if price1 < price2:
        res = (255, 0, 0)
    elif price1 > price2:
        res = (0, 255, 0)
    else:
        res = (0, 0, 255)

    return res

while run:
    delta_time = time.time() - last_time
    while delta_time < time_per_frame:
        delta_time = time.time() - last_time

        for event in pygame.event.get():
            if event.type == QUIT:
                run = False

        keys = pygame.key.get_pressed()
        if keys[pygame.K_ESCAPE]:
            run = False

    fenetre.fill((0, 0, 0))

    reponse = session.get(URL)
    data = json.loads(reponse.text)

    btcMin = float(data["price"]) - DISTANCE / 2
    btcMax = float(data["price"]) + DISTANCE / 2

    if len(listePrice) == NBR:
        listePrice.pop(0)
        listePrice.append(data["price"])
    else:
        listePrice.append(data["price"])

    for i in range(0, len(listePrice)):
        x = i * (WINDOWSIZE[0] / NBR)
        y = heightByLimit(data["price"])

        pygame.draw.circle(fenetre, (255, 255, 255), (x, y), 1, 1)

        if i != 0:
            lineX = (i - 1) * (WINDOWSIZE[0] / NBR)
            newValue = (float(listePrice[i - 1]) - btcMin) / (btcMax - btcMin)
            denum = WINDOWSIZE[0]
            r = -0 / denum
            s = 1 / denum
            lineY = (newValue - r) / s

            pygame.draw.line(fenetre, getColorBetweenPrice(float(listePrice[i - 1]), float(listePrice[i])), (lineX, lineY), (x, y))

    drawText(str(round(float(data["price"]), 2)) + "€", (255, 255, 255), WINDOWSIZE[0] / 2, 20, True)
    drawText(str(round(float(listePrice[len(listePrice) - 2]) - float(listePrice[len(listePrice) - 1]), 2)) + "€", getColorBetweenPrice(listePrice[len(listePrice) - 2], listePrice[len(listePrice) - 1]), WINDOWSIZE[0] / 2, 40, True)
    drawText("ESC: leave", (255, 255, 255), 5, WINDOWSIZE[1] - 20)

    pygame.display.update()

    last_time = time.time()
    frameCount += 1